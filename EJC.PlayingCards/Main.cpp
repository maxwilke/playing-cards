
// Playing Cards
// Ethan Corrigan

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum class Suit
{
	Diamonds,
	Hearts,
	Spades,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

Card HighCard(Card card1, Card card2)
{
	if ((int)card1.rank > (int)card2.rank)
	{
		return card1;
	}
	else if ((int)card1.rank < (int)card2.rank)
	{
		return card2;
	}
	else
	{
		// this will be saved for the tie mechanic
	}
}

void PrintCard(Card card)
{
	// adding an array of names to store for the selected card
	string rankName[] = {"" ,"","Two",
	"Three",
	"Four",
	"Five",
	"Six",
	"Seven",
	"Eight",
	"Nine",
	"Ten",
	"Jack",
	"Queen",
	"King",
	"Ace" };

	string suitName[] = { "Dimonds", "Hearts", "Spades", "Clubs" };


	cout << "The " << rankName[(int)card.rank] << " of " << suitName[(int)card.suit] << "\n";
}
// Max was here -- forked from Ethan

int main()
{
	Card c1;
	c1.rank = Rank::Two;
	c1.suit = Suit::Clubs;

	Card c2;
	c2.rank = Rank::Three;
	c2.suit = Suit::Hearts;
	PrintCard(c1);
	PrintCard(c2);
	HighCard(c1, c2);


	(void)_getch();
	return 0;
}
